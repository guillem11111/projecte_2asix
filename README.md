Documentació:
Kubernetes serveix com a orquestrador de servidors.

Containers: 
Serveixen per a qualsevol màquina.
Es poden manejar molts servidors simultàniament.
Alta disponibilitat (Crea repliqués).
Si falla torna a crear un de nou.

Arquitectura:

Hi ha dos tipus els Workers i els Masters:
Masters:
    - API SERVER (interfaces web).
    - Controler Manager- Sheduler (Rep ordres del Controler Manager).
    - Base de dades (ETCD).

Workers:
    - POD (Lloc de containers amb la mateixa IP, IP dinàmica).

Serveis:
    - Cluster IP: IP Fixa dins del clouster es un petit LoadBalancer.
    - Node Port: Crea un port a cada node que rep el tràfic i l'envia al pod que vulguis.
    - LoadBalancer: Crea un balançador de carrega en el proveidor del nuvol i el redirecciona als pods.

POD: Set de contenidors.

Ordres:

Canviar memoria del minikube:
root@debian:~# minikube config set memory 4096
❗  These changes will take effect upon a minikube delete and then a minikube start

Crear perfil en minikube:
root@debian:~# minikube start --force -p guillem
😄  [guillem] minikube v1.26.1 on Debian bookworm/sid
❗  minikube skips various validations when --force is supplied; this may lead to unexpected behavior
✨  Automatically selected the docker driver. Other choices: ssh, none
🛑  The "docker" driver should not be used with root privileges. If you wish to continue as root, use --force.
💡  If you are running minikube within a VM, consider using --driver=none:
📘    https://minikube.sigs.k8s.io/docs/reference/drivers/none/
📌  Using Docker driver with root privileges
👍  Starting control plane node guillem in cluster guillem
🚜  Pulling base image ...
🔥  Creating docker container (CPUs=2, Memory=4096MB) ...
🐳  Preparing Kubernetes v1.24.3 on Docker 20.10.17 ...
    ▪ Generating certificates and keys ...
    ▪ Booting up control plane ...
    ▪ Configuring RBAC rules ...
🔎  Verifying Kubernetes components...
    ▪ Using image gcr.io/k8s-minikube/storage-provisioner:v5
🌟  Enabled addons: storage-provisioner, default-storageclass
🏄  Done! kubectl is now configured to use "guillem" cluster and "default" namespace by default

Veure configuració de minikube:
root@debian:~# minikube config
config modifies minikube config files using subcommands like "minikube config set driver kvm2"
Configurable fields: 

 * driver
 * vm-driver
 * container-runtime
 * feature-gates
 * v
 * cpus
 * disk-size
 * host-only-cidr
 * memory
 * log_dir
 * kubernetes-version
 * iso-url
 * WantUpdateNotification
 * WantBetaUpdateNotification
 * ReminderWaitPeriodInHours
 * WantNoneDriverWarning
 * WantVirtualBoxDriverWarning
 * profile
 * bootstrapper
 * insecure-registry
 * hyperv-virtual-switch
 * disable-driver-mounts
 * cache
 * EmbedCerts
 * native-ssh
 * rootless
 * MaxAuditEntries

Available Commands:
  defaults      Lists all valid default values for PROPERTY_NAME
  get           Gets the value of PROPERTY_NAME from the minikube config file
  set           Sets an individual value in a minikube config file
  unset         unsets an individual value in a minikube config file
  view          Display values currently set in the minikube config file

Usage:
  minikube config SUBCOMMAND [flags] [options]

Use "minikube <command> --help" for more information about a given command.
Use "minikube options" for a list of global command-line options (applies to all commands).

Possar perfil de minikube en minikube:
root@debian:~# minikube profile minikube
✅  minikube profile was successfully set to minikube

Eliminar minikube:
root@debian:~# minikube delete
🔥  Deleting "minikube" in docker ...
🔥  Deleting container "minikube" ...
🔥  Removing /root/.minikube/machines/minikube ...
💀  Removed all traces of the "minikube" cluster.

Help: kubectl --help
Per veure els name space: 
root@debian:~# kubectl get ns
NAME              STATUS   AGE
default           Active   124m
kube-node-lease   Active   124m
kube-public       Active   124m
kube-system       Active   124m

Per veure els nodes: 
root@debian:~# kubectl get nodes
NAME       STATUS   ROLES           AGE    VERSION
minikube   Ready    control-plane   125m   v1.24.3

Per veure els pods de un ns: 
root@debian:~# kubectl -n kube-system get pods
NAME                               READY   STATUS    RESTARTS      AGE
coredns-6d4b75cb6d-gz2dd           1/1     Running   3 (93s ago)   125m
etcd-minikube                      1/1     Running   3 (25m ago)   125m
kube-apiserver-minikube            1/1     Running   3 (93s ago)   68m
kube-controller-manager-minikube   1/1     Running   3 (93s ago)   125m
kube-proxy-zf6x8                   1/1     Running   3 (93s ago)   125m
kube-scheduler-minikube            1/1     Running   3 (93s ago)   125m
storage-provisioner                1/1     Running   6 (45s ago)   125m

Veure els logs d'un pod:
root@debian:/home/guest/kubernetes/projecte# kubectl logs debian


Perquè mostri mes informació: 
root@debian:~# kubectl -n kube-system get pods -o wide
NAME                               READY   STATUS    RESTARTS       AGE    IP             NODE       NOMINATED NODE   READINESS GATES
coredns-6d4b75cb6d-gz2dd           1/1     Running   3 (109s ago)   125m   172.17.0.2     minikube   <none>           <none>
etcd-minikube                      1/1     Running   3 (25m ago)    126m   192.168.49.2   minikube   <none>           <none>
kube-apiserver-minikube            1/1     Running   3 (109s ago)   68m    192.168.49.2   minikube   <none>           <none>
kube-controller-manager-minikube   1/1     Running   3 (109s ago)   126m   192.168.49.2   minikube   <none>           <none>
kube-proxy-zf6x8                   1/1     Running   3 (109s ago)   125m   192.168.49.2   minikube   <none>           <none>
kube-scheduler-minikube            1/1     Running   3 (109s ago)   126m   192.168.49.2   minikube   <none>           <none>
storage-provisioner                1/1     Running   6 (61s ago)    125m   192.168.49.2   minikube   <none>           <none>

Eliminar un pod:
root@debian:~# kubectl -n kube-system delete pod kube-scheduler-minikube
pod "kube-scheduler-minikube" deleted
Depen de la configuració tonra a crear un altre o no. En aquest cas si.
root@debian:~# kubectl -n kube-system get pods -o wide
NAME                               READY   STATUS    RESTARTS        AGE    IP             NODE       NOMINATED NODE   READINESS GATES
coredns-6d4b75cb6d-gz2dd           1/1     Running   3 (3m25s ago)   127m   172.17.0.2     minikube   <none>           <none>
etcd-minikube                      1/1     Running   3 (27m ago)     127m   192.168.49.2   minikube   <none>           <none>
kube-apiserver-minikube            1/1     Running   3 (3m25s ago)   69m    192.168.49.2   minikube   <none>           <none>
kube-controller-manager-minikube   1/1     Running   3 (3m25s ago)   127m   192.168.49.2   minikube   <none>           <none>
kube-proxy-zf6x8                   1/1     Running   3 (3m25s ago)   127m   192.168.49.2   minikube   <none>           <none>
kube-scheduler-minikube            1/1     Running   3 (3m25s ago)   38s    192.168.49.2   minikube   <none>           <none>
storage-provisioner                1/1     Running   6 (2m37s ago)   127m   192.168.49.2   minikube   <none>           <none>

Per aplicar un archiu yaml que crea un pod:
root@debian:/home/guest/kubernetes# kubectl apply -f 01-pod.yaml 
pod/nginx created

Per veure els pods:
root@debian:/home/guest/kubernetes# kubectl get pods
NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          31s

Per aplicar una orde dins del container:
root@debian:/home/guest/kubernetes# kubectl exec -it nginx -- sh
/ # ls
bin                   docker-entrypoint.sh  lib                   opt                   run                   sys                   var
dev                   etc                   media                 proc                  sbin                  tmp
docker-entrypoint.d   home                  mnt                   root                  srv                   usr
/ # ps ax
PID   USER     TIME  COMMAND
    1 root      0:00 nginx: master process nginx -g daemon off;
   32 nginx     0:00 nginx: worker process
   33 nginx     0:00 nginx: worker process
   34 nginx     0:00 nginx: worker process
   35 nginx     0:00 nginx: worker process
   36 nginx     0:00 nginx: worker process
   37 nginx     0:00 nginx: worker process
   38 nginx     0:00 nginx: worker process
   39 nginx     0:00 nginx: worker process
   40 root      0:00 sh
   47 root      0:00 ps ax

Vuere el estat del pod:
root@debian:/home/guest/kubernetes# kubectl get pod nginx
NAME    READY   STATUS    RESTARTS   AGE
nginx   1/1     Running   0          30s
Amb detall:
root@debian:/home/guest/kubernetes# kubectl get pod nginx -o yaml
apiVersion: v1
kind: Pod
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"v1","kind":"Pod","metadata":{"annotations":{},"name":"nginx","namespace":"default"},"spec":{"containers":[{"image":"nginx:alpine","name":"nginx"}]}}
  creationTimestamp: "2022-09-14T11:30:38Z"
  name: nginx
  namespace: default
  resourceVersion: "4581"
  uid: 6f021032-a5d2-4f92-abe4-769901a97593
spec:
  containers:
  - image: nginx:alpine
    imagePullPolicy: IfNotPresent
    name: nginx
    resources: {}
    terminationMessagePath: /dev/termination-log
    terminationMessagePolicy: File
    volumeMounts:
    - mountPath: /var/run/secrets/kubernetes.io/serviceaccount
      name: kube-api-access-2cccr
      readOnly: true
  dnsPolicy: ClusterFirst
  enableServiceLinks: true
  nodeName: minikube
  preemptionPolicy: PreemptLowerPriority
  priority: 0
  restartPolicy: Always
  schedulerName: default-scheduler
  securityContext: {}
  serviceAccount: default
  serviceAccountName: default
  terminationGracePeriodSeconds: 30
  tolerations:
  - effect: NoExecute
    key: node.kubernetes.io/not-ready
    operator: Exists
    tolerationSeconds: 300
  - effect: NoExecute
    key: node.kubernetes.io/unreachable
    operator: Exists
    tolerationSeconds: 300
  volumes:
  - name: kube-api-access-2cccr
    projected:
      defaultMode: 420
      sources:
      - serviceAccountToken:
          expirationSeconds: 3607
          path: token
      - configMap:
          items:
          - key: ca.crt
            path: ca.crt
          name: kube-root-ca.crt
      - downwardAPI:
          items:
          - fieldRef:
              apiVersion: v1
              fieldPath: metadata.namespace
            path: namespace
status:
  conditions:
  - lastProbeTime: null
    lastTransitionTime: "2022-09-14T11:30:38Z"
    status: "True"
    type: Initialized
  - lastProbeTime: null
    lastTransitionTime: "2022-09-14T11:30:39Z"
    status: "True"
    type: Ready
  - lastProbeTime: null
    lastTransitionTime: "2022-09-14T11:30:39Z"
    status: "True"
    type: ContainersReady
  - lastProbeTime: null
    lastTransitionTime: "2022-09-14T11:30:38Z"
    status: "True"
    type: PodScheduled
  containerStatuses:
  - containerID: docker://697e1ccf3410456f919ea65fd7052642b8ade20b7e7fd886aac0cbd1d7859b6a
    image: nginx:alpine
    imageID: docker-pullable://nginx@sha256:082f8c10bd47b6acc8ef15ae61ae45dd8fde0e9f389a8b5cb23c37408642bf5d
    lastState: {}
    name: nginx
    ready: true
    restartCount: 0
    started: true
    state:
      running:
        startedAt: "2022-09-14T11:30:39Z"
  hostIP: 192.168.49.2
  phase: Running
  podIP: 172.17.0.3
  podIPs:
  - ip: 172.17.0.3
  qosClass: BestEffort
  startTime: "2022-09-14T11:30:38Z"

Eliminar un pod:
root@debian:/home/guest/kubernetes# kubectl delete pod nginx
pod "nginx" deleted

Per veure l'estat i els events que pasen al pod:
root@debian:/home/guest/kubernetes# kubectl describe pod hello-v1-849d7b678-5c7rw
Name:             hello-v1-849d7b678-5c7rw
Namespace:        default
Priority:         0
Service Account:  default
Node:             minikube/192.168.49.2
Start Time:       Wed, 14 Sep 2022 21:21:55 +0200
Labels:           app=hello-v1
                  pod-template-hash=849d7b678
Annotations:      <none>
Status:           Running
IP:               172.17.0.7
IPs:
  IP:           172.17.0.7
Controlled By:  ReplicaSet/hello-v1-849d7b678
Containers:
  hello:
    Container ID:   docker://f5fd6e85f7bceecaafc9ade226accf81557089f26099a6df8e3e554ccc33b329
    Image:          gcr.io/google-samples/hello-app:1.0
    Image ID:       docker-pullable://gcr.io/google-samples/hello-app@sha256:88b205d7995332e10e836514fbfd59ecaf8976fc15060cd66e85cdcebe7fb356
    Port:           8080/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Fri, 16 Sep 2022 11:27:46 +0200
    Last State:     Terminated
      Reason:       Error
      Exit Code:    255
      Started:      Wed, 14 Sep 2022 21:21:56 +0200
      Finished:     Fri, 16 Sep 2022 11:27:28 +0200
    Ready:          True
    Restart Count:  1
    Limits:
      cpu:     500m
      memory:  128Mi
    Requests:
      cpu:        200m
      memory:     64Mi
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-x2b9m (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  kube-api-access-x2b9m:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   Burstable
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason          Age   From     Message
  ----    ------          ----  ----     -------
  Normal  SandboxChanged  45s   kubelet  Pod sandbox changed, it will be killed and re-created.
  Normal  Pulled          45s   kubelet  Container image "gcr.io/google-samples/hello-app:1.0" already present on machine
  Normal  Created         45s   kubelet  Created container hello
  Normal  Started         45s   kubelet  Started container hello

Permet veure pods,servies, etc... :
root@debian:/home/guest/kubernetes# kubectl get all
NAME                           READY   STATUS    RESTARTS        AGE
pod/hello-v1-849d7b678-5c7rw   1/1     Running   1 (5m38s ago)   38h
pod/hello-v1-849d7b678-hgdw2   1/1     Running   1 (5m38s ago)   38h
pod/hello-v1-849d7b678-tdndl   1/1     Running   1 (5m38s ago)   38h
pod/hello-v2-5c8979fb8-rv2xw   1/1     Running   1 (36h ago)     38h
pod/hello-v2-5c8979fb8-vlrvv   1/1     Running   1 (5m38s ago)   38h
pod/hello-v2-5c8979fb8-x4sf5   1/1     Running   1 (5m38s ago)   38h
pod/ubuntu                     1/1     Running   1 (5m38s ago)   39h

NAME                   TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
service/hello-v1-svc   ClusterIP   10.101.207.235   <none>        80/TCP    38h
service/hello-v2-svc   ClusterIP   10.109.1.177     <none>        80/TCP    38h
service/kubernetes     ClusterIP   10.96.0.1        <none>        443/TCP   2d

NAME                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/hello-v1   3/3     3            3           38h
deployment.apps/hello-v2   3/3     3            3           38h

NAME                                 DESIRED   CURRENT   READY   AGE
replicaset.apps/hello-v1-849d7b678   3         3         3       38h
replicaset.apps/hello-v2-5c8979fb8   3         3         3       38h

Per veure un servei:
root@debian:/home/guest/kubernetes# kubectl describe svc hello-v1-svc
Name:              hello-v1-svc
Namespace:         default
Labels:            <none>
Annotations:       <none>
Selector:          app=hello-v1
Type:              ClusterIP
IP Family Policy:  SingleStack
IP Families:       IPv4
IP:                10.101.207.235
IPs:               10.101.207.235
Port:              <unset>  80/TCP
TargetPort:        8080/TCP
Endpoints:         172.17.0.11:8080,172.17.0.7:8080,172.17.0.8:8080
Session Affinity:  None
Events:            <none>

Permet veure el PVC (Persiten Volume Clain, Es una demanda d'un disc desde kubernetes fins el proveidor.)
root@debian:/home/guest/kubernetes# kubectl get pvc
NAME                       STATUS    VOLUME   CAPACITY   ACCESS MODES   STORAGECLASS       AGE
csi-pvc-my-csi-app-set-0   Pending                                      do-block-storage   40h

Archius .yaml:
Els elements principals són:
  - apiVersion i kind: Depenen del tipus de kind que vols varia la apiVersion.
  - metadata: Etiquetes o noms del pod.
  - spec: Containers(nom,imatge,varibles d'entorn, recursos que poden ser request que son els grantizats i els limits que es el limit que el pod pot utilitzar si el supera, el matara linux no kubernetes, volumeMounts serveix per montar coses, tambe es poden passar aguments igual que amb l'orde docker run,).
  - readinessProbe: Forma d'explicar a kubernetes que el teu pod pot rebre trafic.
  - livenessProbe: Forma d'explicar a kubernetes que el teu pod esta viu.
  - També es poden possar els ports.
  - Replicas: cantitat de pods que volen al deployment.
  - Deployment: Template per crear pods.
  - volumeClaimTemplates: Has de possar les opcions de muntatge.
  

Bibliografia:

Tutorial: https://kubernetes.io/es/docs/tutorials/hello-minikube/
Instalar Minikube: https://minikube.sigs.k8s.io/docs/start/
Instal·lar Minikube: https://minikube.sigs.k8s.io/docs/start/
informació kubernetes: https://www.youtube.com/watch?v=DCoBcpOA7W4
